function FBImage() {
}

FBImage._extends(AbstractObject);

FBImage.prototype.init = function (tag, parent) {

    this.bind('button.fb-login', 'click', this.onFBImageButtonClicked);

};

//private
FBImage.prototype.onFBImageButtonClicked = function () {

    if (!FacebookController.getInstance().model.uid) {
        this.bind(FacebookController.getInstance().model, FacebookEvent.ON_SESSION, this.onSession);
        FacebookController.getInstance().login(['user_photos']);
    }
    else {
        this.onSession();
    }
};

FBImage.prototype.onSession = function () {
    jQuery('button.fb-login').addClass('element-invisible');
    jQuery('input.fb-upload').removeClass('element-invisible');
};
