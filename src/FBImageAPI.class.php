<?php

class FBImageAPI {

  public static function getAlbumId($type) {
    $session = FacebookLightAPI::init();

    if (!$session) {
      return NULL;
    }

    $request = new Facebook\FacebookRequest($session, 'GET', '/me/albums');
    try {
      $response = $request->execute();
      $response = $response->getResponse();

      foreach ($response->data as $album) {
        if ($album->type == $type) {
          return $album;
        }
      }
    } catch (Exception $ex) {
    }

    return NULL;
  }

  //TODO: handle $offset and $count
  public static function getAlbumPhotos($albumId, $offset = 0, $count = 10) {
    $session = FacebookLightAPI::init();

    if (!$session) {
      return NULL;
    }

    $request = new Facebook\FacebookRequest($session, 'GET', '/' . $albumId . '/photos');
    try {
      $response = $request->execute();
      $response = $response->getResponse();
      return $response->data;
    } catch (Exception $ex) {
    }
    return NULL;
  }

  public static function getPhoto($photoId) {
    $session = FacebookLightAPI::init();
    if (!$session) {
      return NULL;
    }

    $request = new Facebook\FacebookRequest($session, 'GET', '/' . $photoId);
    try {
      $response = $request->execute();
      $response = $response->getGraphObject();
      $response = $response->asArray();

      $file = system_retrieve_file($response['source'], NULL, TRUE, FILE_EXISTS_REPLACE);
      $file->status = 0;
      file_save($file);
      return $file;
    } catch (Exception $ex) {
    }

    return NULL;
  }
}
